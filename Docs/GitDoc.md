1. Installation of Git in WSL

```bash
sudo apt update
sudo apt install git 
```

2. Creating gitlab repository.

3. Creating SSH key and cloning with SHH

```bash
# Generating SSH Key
ssh-keygen -t ed25519 -C "email@email.com"

# Evaluating the agents
eval "$(ssh-agent -s)"

#adding agent 
ssh-add ~/.ssh/yourkeyname

#copy the SSH key
xclip -sel clip < publickeyname

#if you dont have xclip in wsl, do
sudo apt-get update
sudo apt-get install xclip

#paste the SSH key in gitlab
 
#cloning git repo using ssh
git clone reponame

```

4. Working with git 

```bash
# DO git pull to update with your remote repository
git pull

# to check the status of you local git repo
git status

#to stage the changed file
git add .

#to add the change to git 
git commit -a -m "commit message"

#to push your local work to remote repo
git push
```
