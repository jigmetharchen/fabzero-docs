# kicad Scametic Digram

Test Circuit 

![initial circuit](../kicad/screenshot/firstCircuit.png)

My Circuit with push button

![my circuit](../kicad/screenshot/mycircuit.png)


PCB design

![PCB](../kicad/screenshot/pcb.png)