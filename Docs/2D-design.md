# 2D-Design

JPG/PNG --> vactor image --> Raster Image (600)

Difference between Raster vs vector image

![image](../images/rastervsvector.gif)

Original Image

![png image](../images/rasterimage.jpg)

Vector Image

![vector image](../images/drawing.svg) 

Back to raster image with 600 dpi

![raster](../images/vactor.png)

