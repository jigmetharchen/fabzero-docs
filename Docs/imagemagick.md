# Imagemagick CLI

### Installation

```bash
sudo apt install imagemagick
```
### Image conversion (resize)

```bash
convert image1.png -resize 640X640 resizedimage1.png 
```
> Actual Image 

![actual Image](../images/smartwork.jpg)

> Resized image
```bash
convert image1.jpg -resize 640X640 resizedname.jpg
```

![Resized Image](../images/Resizedsmartwork.jpg)


> concatenate two or more image

```bash
convert image1.jpg image2.jpg -geometry x400 +append renamed.jpg
```

![concat Image](../images/concat.jpg)

