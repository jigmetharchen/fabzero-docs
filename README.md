# Fab Zero Documentation 

### [Introduction](./Docs/aboutme.md)

> Hello to the wonderful soul visiting my repo. I am Jigme Tharchen . [_Click here_](./Docs/aboutme.md) for more information.

### Final Project
> As the part of fab-zero program, the final project i have chosen is a sound controlled auto on/off power switch. For more detail [_click here_](./Docs/FinalProject.md)

### Git Documentation

> Git is a distributed version control system which keeps the records of changes to a file. The [_Full documentation_](./Docs/GitDoc.md) of installation and process of generating SSH key is documented [_here_](./Docs/GitDoc.md) 

### Flameshot screenshot tool

> Flameshot is an open source software which allows in-app screenshot editing like arrow mark, highlight test, blur a section, etc. please use the following [_download link_](https://github.com/flameshot-org/flameshot/releases/tag/v11.0.0)

### Imagemagick CLI 

> Imagemagick is great command line tool for converting, modifing, and editing raster image. [_Click here_](./Docs/imagemagick.md) for my work with imagemagick CLI.  


### 2D Design with inkscape

> For 2D design have to used inkscape, [_download link here_](https://inkscape.org/release/inkscape-1.1.1/).
You can view my work [_here._](./Docs/2D-design.md)

### Circuit Scametic Design with KiCAD

> the screenshot of my circuit is [_displayed here_](./Docs/kicad.md)
